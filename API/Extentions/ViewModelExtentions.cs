﻿using API.Models;
using DATA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Extentions
{
	public static class ViewModelExtentions
	{
		public static TableContent ToTableContent(this Table2 table)
		{
			if (table.Table1 == null)
			{
				throw new ArgumentNullException(paramName: "table");
			}
			var content = new TableContent
			{
				Code = table.Code,
				Name = table.Name,
				Action = table.Action,
				Comments = table.Comments,
				Definition = table.Table1.Definition,
				Meaning = table.Table1.Meaning,
				ResponseCode = table.Table1.ResponseCode.ToString()
			};
			return content;
		}

		public static IEnumerable<TableContent> ToTableContent(this IEnumerable<Table2> tables)
		{
			return tables.Select(table => table.ToTableContent());
		}
	}
}