﻿using System.Web.Http;

namespace API.Controllers
{
    public class HomeController : ApiController
    {
		[HttpGet]
		public IHttpActionResult Home()
		{
			return Ok();
		}
    }
}
