﻿using API.Extentions;
using API.Models;
using DATA;
using DATA.Repositories;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Controllers
{
	[RoutePrefix("api/Data")]
	public class DataController : ApiController
	{
		private readonly IRepository<Table2> _repo;
		private readonly IActions _actionService;

		public DataController(IRepository<Table2> repo, IActions actionService)
		{
			_repo = repo;
			_actionService = actionService;
		}
		/// <summary>
		/// Returns all data from database. For testing purposes.
		/// </summary>
		/// <returns></returns>
		[HttpGet, Route(""), ResponseType(typeof(CodesViewModel))]
		public IHttpActionResult GetAll()
		{
			var all = _repo.GetAll();
			return Content(HttpStatusCode.OK, CreateResponse(all, "All Data Received"));
		}
		/// <summary>
		/// Takes 3 digit code as parameter and returns content matching this code.
		/// </summary>
		/// <param name="code">code</param>
		/// <returns></returns>
		[HttpGet, Route(@"{code:regex(^\d{3}$)}"), ResponseType(typeof(CodeViewModel))]
		public HttpResponseMessage GetTableContent(string code)
		{
			var result = _repo.GetByCode(code);
			if (result == null) return Request.CreateResponse(HttpStatusCode.NotFound);
			if (result.Action == "None")
			{
				return Request.CreateResponse(HttpStatusCode.OK, CreateResponse(result, "Поздравляем, ваш запрос принят успешно"));
			}
			var actionResult = _actionService.ExecuteAction(result.Action);
			return Request.CreateResponse(HttpStatusCode.OK, CreateResponse(result, actionResult));
		}

		#region Helpers
		/// <summary>
		/// Helper method to convert data to more manageable format for web-client.
		/// </summary>
		/// <param name="data">Content</param>
		/// <param name="comment">Message</param>
		/// <returns></returns>
		private static CodeViewModel CreateResponse(Table2 data, string comment = "")
		{
			var response = new CodeViewModel
			{
				Content = data.ToTableContent(),
				Message = comment
			};
			return response;
		}
		/// <summary>
		/// Helper method to convert data to more manageable format for web client.
		/// </summary>
		/// <param name="data">Content</param>
		/// <param name="comment">Message</param>
		/// <returns></returns>
		private static CodesViewModel CreateResponse(List<Table2> data, string comment = "")
		{
			var response = new CodesViewModel
			{
				Content = data.ToTableContent(),
				Message = comment
			};
			return response;
		}
		#endregion
	}
}
