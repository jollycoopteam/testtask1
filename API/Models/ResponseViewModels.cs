﻿using System.Collections.Generic;

namespace API.Models
{
	public class CodeViewModel
	{
		public TableContent Content { get; set; }
		public string Message { get; set; }
	}

	public class CodesViewModel
	{
		public IEnumerable<TableContent> Content { get; set; }
		public string Message { get; set; }
	}

	public class TableContent
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public string Action { get; set; }
		public string Comments { get; set; }
		public string Definition { get; set; }
		public string Meaning { get; set; }
		public string ResponseCode { get; set; }
	}
}