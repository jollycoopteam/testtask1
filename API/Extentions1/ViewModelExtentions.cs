﻿using API.Models;
using DATA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Extentions
{
	public static class ViewModelExtentions
	{
		public static DataTable ToDataTable(this Table_2 table)
		{
			if (table.Table_1 == null)
			{
				throw new ArgumentNullException("table");
			}

			var tables = new DataTable
			{
				Table2 = table,
				Table1 = table.Table_1
			};
			return tables;
		}
		public static IEnumerable<DataTable> ToDataTable(this List<Table_2> tables)
		{
			foreach (var table in tables)
			{
				yield return table.ToDataTable();
			}
		}
	}

}