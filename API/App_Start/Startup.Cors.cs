﻿using Microsoft.Owin.Cors;
using Owin;
using System.Threading.Tasks;
using System.Web.Cors;

namespace API
{
	public partial class Startup
	{
		/// <summary>
		/// Enables cross-browser resource sharing for service.
		/// </summary>
		/// <param name="app"></param>
		private static void EnableCors(IAppBuilder app)
		{
			var corsPolicy = CreateCorsPolicy();

			app.UseCors(options: new CorsOptions
			{
				PolicyProvider = new CorsPolicyProvider
				{
					PolicyResolver = ctx => Task.FromResult(corsPolicy)
				}
			});
		}
		/// <summary>
		/// Creates CORS policy object with necessary constraints.
		/// </summary>
		/// <param name="allowAnyHeader">allows any headers</param>
		/// <param name="allowAnyMethod">allows any methods</param>
		/// <param name="allowAnyOrigin">allows any origins</param>
		/// <param name="supportCredentials">suppoorts credentials</param>
		/// <returns></returns>
		private static CorsPolicy CreateCorsPolicy(bool allowAnyHeader = true,
			bool allowAnyMethod = true,
			bool allowAnyOrigin = true,
			bool supportCredentials = true)
		{
			var policy =  new CorsPolicy
			{
				// Configure Cors Policy
				AllowAnyHeader = allowAnyHeader,
				AllowAnyMethod = allowAnyMethod,
				AllowAnyOrigin = allowAnyOrigin,
				SupportsCredentials = supportCredentials
			};
			//other config here
			return policy;
		}
	}
}