﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Autofac.Integration.WebApi;

[assembly: OwinStartup(typeof(API.Startup))]

namespace API
{
	public partial class Startup
	{
		/// <summary>
		/// Applicaiton startup. 
		/// </summary>
		/// <param name="app"></param>
		public void Configuration(IAppBuilder app)
		{
			var config = new HttpConfiguration();

			ConfigureApi(config);

			var container = BuildContainer();
			config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

			EnableCors(app);

			app.UseAutofacMiddleware(container);
			app.UseAutofacWebApi(config);
			app.UseWebApi(config);
			app.UseWelcomePage();
		}
	}
}
