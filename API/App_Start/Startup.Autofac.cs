﻿using Autofac;
using Autofac.Integration.WebApi;
using DATA;
using DATA.Repositories;
using System.Reflection;

namespace API
{
	public partial class Startup
	{
		/// <summary>
		/// Configure IoC Autofac container.
		/// </summary>
		/// <returns></returns>
		private static IContainer BuildContainer()
		{
			var builder = new ContainerBuilder();
			WireUp(builder);
			return builder.Build();
		}
		/// <summary>
		/// Registration.
		/// </summary>
		/// <param name="builder">autofac container builder</param>
		private static void WireUp(ContainerBuilder builder)
		{
			builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
			builder.RegisterType<Actions>().As<IActions>().InstancePerRequest();
			builder.RegisterType<DataRepository>().As<IRepository<Table2>>().InstancePerRequest();
		}
	}
}