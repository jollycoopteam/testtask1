﻿using System.Web.Http;

namespace API
{
	public partial class Startup
	{
		/// <summary>
		/// Standart web api configuration.
		/// </summary>
		/// <param name="config">web api configuration class</param>
		private static void ConfigureApi(HttpConfiguration config)
		{
			ConfigureRoutes(config);
		}
		/// <summary>
		/// Configures routes
		/// </summary>
		/// <param name="config">web api configuration class</param>
		private static void ConfigureRoutes(HttpConfiguration config)
		{
			// route-based configuration
			config.MapHttpAttributeRoutes();
			// convention-based configuration.
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{action}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
		}
	}
}