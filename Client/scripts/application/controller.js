﻿(function () {
    "use strict";
    angular.module("app").controller("Codes", Codes);

    Codes.$inject = ["$http", "codesService"];

// ReSharper disable once InconsistentNaming
    function Codes($http, codesService) {
        var vm = this;
        var codes = codesService;
        vm.input = "";
        vm.is = {
            error: {
                processError: false,
                serverError: false,
                validError: false
            },
            data: {
                available: false,
                success: false
            }
        };
        vm.err = {
            process_error: "An error occured while processing your request.",
            server_error: "Something with our server! I`m sorry. Please try again later.",
            valids: {
                not_null: "Value must not be empty",
                too_short: "Value must not be less than 3 chars long",
                too_long: "Value must not be more than 3 chars long",
                pattern: "Invalid characters. Only 3 numbers"
            },
            valid_error: ""
        };
        vm.success = "";
        vm.table = {
            headings: ["Properties", "Values"],
            body: []
        };
        vm.regex = "[0-9]{3,}";
        vm.getCode = getCode;
        vm.get = getCode;
        ////////////////

        /**
         * @description Gets code data from server using codesService.
         * @param {Object} form - validation object
         */
        function getCode(form) {
            if (form.$valid) {
                reset();
                codes.getCode(vm.input)
                    .then(function (response) {
                        // handle successfull result
                        populateTable(response.data);
                        vm.is.data.available = true;
                        vm.is.data.success = true;
                    }, function (response) {
                        handleErrors(response.status);
                    });
            }
        }
        /**
         * @description populates html tables with received data
         * @param {Object} data - data from response object
         */
        function populateTable(data) {
            var obj = new ResponseObj(data.Message, data.Content);
            for (var name in obj.content) {
                if (obj.content.hasOwnProperty(name)) {
                    vm.table.body.push({ key: name, value: obj.content[name] });
                }
            }
            vm.success = obj.message;
        }
        /**
         * @description handles errors from server
         * @param {any} status - status from response object
         */
        function handleErrors(status) {
            switch (status) {
                case 400:
                    vm.is.error.processError = true;
                    break;
                case 404:
                    vm.is.error.validError = true;
                    vm.err.valid_error = "No matching content was found";
                    break;
                case 500:
                    vm.is.error.serverError = true;
                    break;
                default:
                    break;
            }
        }
        /**
        * @description resets all error models and notifications to initial values.
        */
        function reset() {
            resetErrors();
            resetData();
            resetNotifications();
            function resetErrors() {
                for (var prop in vm.is.error) {
                    if (vm.is.error.hasOwnProperty(prop)) {
                        vm.is.error[prop] = false;
                    }
                }
            }
            /**
             * Clears data.
             */
            function resetData() {
                vm.table.body.length = 0;
            }
            /**
             * resets notifications
             */
            function resetNotifications() {
                for (var prop in vm.is.data) {
                    if (vm.is.data.hasOwnProperty(prop)) {
                        vm.is.data[prop] = false;
                    }
                }
            }
        }
    }
})();