﻿(function () {
    "use strict";
    angular
        .module("app")
        .directive("ngJustenter", function () { // directive for sending data via pressing enter.
            return function (scope, element, attrs) {
                element.bind("keydown keypress", function (event) {
                    if (event.which === 13) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngJustenter, { 'event': event });
                        });
                        event.preventDefault();
                    }
                });
            };
        });
})();
(function () {
    "use strict";
    angular
        .module("app")
        .directive("myValidation", function () { // custom angular validation directive
            return {
                require: "ngModel",
                link: function (scope, element, attr, mCtrl) {
                    function checkLess(value) {
                        var lessThen3 = value.length < 3;
                        if (lessThen3) {
                            mCtrl.$setValidity("lessThen3", false);
                        }
                        else {
                            mCtrl.$setValidity("lessThen3", true);
                        }
                    }
                    function checkMore(value) {
                        var biggerThan3 = value.length > 3;
                        if (biggerThan3) {
                            mCtrl.$setValidity("biggerThen3", false);
                        }
                        else {
                            mCtrl.$setValidity("biggerThen3", true);
                        }
                    }
                    function checkMatched(value) {
                        var regex = new RegExp("^[0-9]{3,3}$");
                        var match = regex.test(value);
                        if (match === false) {
                            mCtrl.$setValidity("Matched", false);
                        }
                        else {
                            mCtrl.$setValidity("Matched", true);
                        }
                    }

                    function isEmpty(str) {
                        return (!str || /^\s*$/.test(str));
                    }

                    function checkEmpty(value) {
                        var empty = isEmpty(value);
                        if (empty) {
                            mCtrl.$setValidity("Empty", false);
                        }
                        else {
                            mCtrl.$setValidity("Empty", true);
                        }
                    }
                    function validate(value) {
                        checkEmpty(value);
                        checkLess(value);
                        checkMore(value);
                        checkMatched(value);
                        return value;
                    }
                    mCtrl.$parsers.push(validate);
                }
            };
        });
})();