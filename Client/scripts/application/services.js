﻿(function () {
    "use strict";
    angular.module("app").service("codesService", codesService);

    codesService.$inject = ["$http"];
    /**
     * @description angular Service for creating requests to a server
	 * @param {Any} $http - $http from angularjs
     */
    function codesService($http) {
        this.url = "http://localhost:49599/api/Data/";
        this.getCode = getCode;
        ////////////////
        /**
         * @description Function for GET request to the server
         * @param {String} code - code from user input
         * @return {Object} getCode
         */
        function getCode(code) {
        // ReSharper disable once MisuseOfOwnerFunctionThis
            return $http.get(this.url + code);
        }
    }
})();