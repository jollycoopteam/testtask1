﻿/**
 * 
 * @param {String} message - message string from server
 * @param {Object} content - content from server
 */
function ResponseObj(message, content) {
	this.message = message;
	this.content = content;
}