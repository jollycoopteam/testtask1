﻿namespace DATA
{
	public interface IActions
	{
		string ExecuteAction(string actionName); 
	}
}
