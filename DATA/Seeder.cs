﻿using System.Collections.Generic;

namespace DATA
{
	public static class Seeder
	{
		/// <summary>
		/// Method for mimicing database with data. Seeds "database" with necessary values.
		/// </summary>
		/// <param name="table">database</param>
		public static void Seed(this List<Table2> table)
		{
			var codeA = new Table1() { ResponseCode = 'A', Meaning = "Approval", Definition = "Authorization is approved" };
			var codeC = new Table1() { ResponseCode = 'C', Meaning = "Call", Definition = "Voice authorization is required" };
			var codeD = new Table1() { ResponseCode = 'D', Meaning = "Decline", Definition = "The approval is declined" };
			var codeE = new Table1() { ResponseCode = 'E', Meaning = "Error", Definition = "A processing error occurred" };

			var code000 = new Table2() { Code = "000", Name = "No Answer", Action = "Resend", Comments = "Received no answer from network", Table1 = codeD };
			var code100 = new Table2() { Code = "100", Name = "Approved", Action = "None", Comments = "Successfully approved", Table1 = codeA };
			var code106 = new Table2() { Code = "106", Name = "Provided Auth", Action = "None", Comments = "Successfully approved", Table1 = codeA };
			var code204 = new Table2() { Code = "204", Name = "Other Error", Action = "Fix", Comments = "Unidentifiable error", Table1 = codeC };
			var code260 = new Table2() { Code = "260", Name = "Soft AVS", Action = "Call", Comments = "Card was authorized, but address did not match", Table1 = codeC };
			var code201 = new Table2() { Code = "201", Name = "Invalid Account Number", Action = "Call", Comments = "Bad check digit or other card problem", Table1 = codeE };
			var code202 = new Table2() { Code = "202", Name = "Bad Amount", Action = "Fix", Comments = "Amount sent was zero or unreadable", Table1 = codeE };
			var code203 = new Table2() { Code = "203", Name = "Zero Amount", Action = "Fix", Comments = "Amount sent was zero", Table1 = codeE };
			table.AddRange(new List<Table2> { code000, code100, code106, code201, code202, code203, code204, code260 });
		}
	}
}
