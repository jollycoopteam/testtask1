﻿using System;
using System.Reflection;
// ReSharper disable UseStringInterpolation

namespace DATA
{
	public class Actions : IActions
	{
		private string _methodName;
		/// <summary>
		/// Takes method name as parameter and executes matching private method via reflection
		/// </summary>
		/// <param name="methodName">method name</param>
		/// <returns></returns>
		public string ExecuteAction(string methodName)
		{
			if (methodName == null)
			{
				throw new ArgumentNullException(paramName: "methodName");
			}
			_methodName = methodName;
			var result = GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance).Invoke(this, null);
			return result.ToString();
		}

		private string Resend()
		{
			return string.Format("Method {0} was successfully executed", _methodName);
		}

		private string Fix()
		{
			return string.Format("Method {0} was successfully executed", _methodName);
		}

		private string Call()
		{
			return string.Format("Method {0} was successfully executed", _methodName);
		}


	}
}
