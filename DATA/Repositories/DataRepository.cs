﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DATA.Repositories
{
	/// <summary>
	/// Repository
	/// </summary>
	public class DataRepository : IRepository<Table2>
	{
		private readonly List<Table2> _db = new List<Table2>();
		public DataRepository()
		{
			_db.Seed();
		}

		public List<Table2> GetAll()
		{
			return _db;
		}

		public Table2 GetByCode(string code)
		{
			return _db.FirstOrDefault(t => t.Code == code);
		}

		public void Delete(Table2 table)
		{
			var entity = _db.FirstOrDefault(t => t.Code == table.Code);
			if (entity != null)
			{
				_db.Remove(entity);
			}
		}

		public void Insert(Table2 table)
		{
			_db.Add(table);
		}

		public void Update(Table2 table)
		{
			if (_db.Any(t => t.Code == table.Code))
			{
				var index = _db.FindIndex(t => t.Code == table.Code);
				_db.ElementAt(index).Action = table.Action;
				_db.ElementAt(index).Code = table.Code;
				_db.ElementAt(index).Comments = table.Comments;
				_db.ElementAt(index).Name = table.Name;
				_db.ElementAt(index).Table1 = table.Table1;
			}
		}
	}
}
