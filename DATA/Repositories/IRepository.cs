﻿using System.Collections.Generic;

namespace DATA.Repositories
{
	public interface IRepository<T>
	{
		T GetByCode(string code);
		List<T> GetAll();
		void Insert(T t);
		void Update(T t);
		void Delete(T t);
	}
}
