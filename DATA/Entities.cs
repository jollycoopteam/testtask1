﻿using System.Collections.Generic;

namespace DATA
{
	public class Table2
	{ 
		public string Code { get; set; }
		public string Name { get; set; }
		public string Action { get; set; }
		public string Comments { get; set; }
		public Table1 Table1 { get; set; }
	}

	public class Table1
	{
		public char ResponseCode { get; set; }
		public string Meaning { get; set; }
		public string Definition { get; set; }

		public ICollection<Table2> Tables2 { get; set; }
	}
}
