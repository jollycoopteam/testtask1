﻿using API.Controllers;
using API.Models;
using DATA;
using DATA.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Http.Results;

namespace API.Tests
{
	[TestClass]
	public class DataControllerTests
	{
		private DataController _ctr;
		private Mock<IRepository<Table2>> _fakeRepo;
		private Mock<IActions> _fakeAtions;

		[TestInitialize]
		public void SetUp()
		{
			_fakeRepo = new Mock<IRepository<Table2>>();
			_fakeAtions = new Mock<IActions>();
		}

		[TestMethod]
		public void GetAllReturnsOk()
		{
			_fakeRepo.Setup(x => x.GetAll()).Returns(new List<Table2>());

			_ctr = ConfigureCtr(_fakeRepo.Object, _fakeAtions.Object);

			var response = _ctr.GetAll();
			var result = response as NegotiatedContentResult<CodesViewModel>;

			Assert.IsNotNull(result);
			Assert.IsTrue(result.StatusCode == System.Net.HttpStatusCode.OK);
			Assert.IsNotNull(result.Content);
		}
		[TestMethod]
		public void GetTableContentsReturnsOk()
		{
			var table = new Table2() { Code = "000", Action = "Resend", Comments = "", Name = "", Table1 = new Table1() };
			_fakeRepo.Setup(x => x.GetByCode(It.IsAny<string>())).Returns(table);
			_fakeAtions.Setup(x => x.ExecuteAction(It.IsAny<string>())).Returns(It.IsAny<string>());

			_ctr = ConfigureCtr(_fakeRepo.Object, _fakeAtions.Object);

			var response = _ctr.GetTableContent("valid data");

			Assert.IsNotNull(response);
			Assert.IsTrue(response.IsSuccessStatusCode);
			Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
		}
		[TestMethod]
		public void GetTableContentsReturnsNotFound()
		{
			_fakeRepo.Setup(x => x.GetByCode(It.IsAny<string>())).Returns<Table2>(null);
			_ctr = ConfigureCtr(_fakeRepo.Object, _fakeAtions.Object);

			var response = _ctr.GetTableContent("invalid data");

			Assert.IsNotNull(response);
			Assert.IsTrue(!response.IsSuccessStatusCode);
			Assert.AreEqual(System.Net.HttpStatusCode.NotFound, response.StatusCode);
		}
		[TestMethod]
		public void ControllerHandlesValidValue()
		{
			var repo = new DataRepository();
			var actions = new Actions();
			_ctr = ConfigureCtr(repo, actions);

			var response = _ctr.GetTableContent("000");

			Assert.IsNotNull(response);
			Assert.IsTrue(response.IsSuccessStatusCode);
			Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
		}

		[TestMethod]
		public void ControllerHandlesInValidValue()
		{
			var repo = new DataRepository();
			var actions = new Actions();
			_ctr = ConfigureCtr(repo, actions);

			var response = _ctr.GetTableContent("");

			Assert.IsNotNull(response);
			Assert.IsTrue(!response.IsSuccessStatusCode);
			Assert.AreEqual(System.Net.HttpStatusCode.NotFound, response.StatusCode);
		}
		[TestMethod]
		public void RegexWorksProperly()
		{
			const string regex = @"^\d{3}$";
			var result = Regex.IsMatch("400", regex);

			Assert.IsTrue(result);
		}
		[TestMethod]
		public void RegexWorksProperlyWithInvalidValue()
		{
			const string regex = @"^\d{3}$";
			var result = Regex.IsMatch("aaaa", regex);

			Assert.IsFalse(result);
		}

		private static DataController ConfigureCtr(IRepository<Table2> repo, IActions actions)
		{
			var ctr = new DataController(repo, actions)
			{
				Configuration = new System.Web.Http.HttpConfiguration(),
				Request = new System.Net.Http.HttpRequestMessage()
			};
			return ctr;
		}
	}
}
