﻿using DATA;
using DATA.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace API.Tests
{
	[TestClass]
	public class DataRepositoryTests
	{
		private IRepository<Table2> _dataRepo;
		[TestInitialize]
		public void SetUp()
		{
			//Arrange
			_dataRepo = new DataRepository();
		}
		[TestMethod]
		public void GetAllReturnsSomething()
		{
			// Act
			var result = _dataRepo.GetAll();
			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(List<Table2>));
		}
		[TestMethod]
		public void GetByCodeReturnsSomething()
		{
			//Arrenge
			const string code = "000";
			//Act
			var result = _dataRepo.GetByCode(code);
			// Assert
			Assert.IsNotNull(result);
			Assert.IsInstanceOfType(result, typeof(Table2));
		}
		[TestMethod]
		public void InsertWorksProperly()
		{
			var count = _dataRepo.GetAll().Count;

			var newData = new Table2();

			_dataRepo.Insert(newData);
			var newCount = _dataRepo.GetAll().Count;
			Assert.IsTrue(newCount > count);
		}
		[TestMethod]
		public void DeleteWorksProperly()
		{
			var toDelete = _dataRepo.GetByCode("000");
			var count = _dataRepo.GetAll().Count;
			_dataRepo.Delete(toDelete);
			var newCount = _dataRepo.GetAll().Count;
			Assert.IsTrue(newCount < count);
		}
		[TestMethod]
		public void UpdateWorkProperly()
		{
			var newData = new Table2() { Code = "000", Name = "No Answer", Action = "UPDATED FIELD", Comments = "Received no answer from network", Table1 = null };
			_dataRepo.Update(newData);

			var item = _dataRepo.GetByCode("000");
			Assert.AreEqual(newData.Action, item.Action);
		}
	}
}
