﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DATA;

namespace API.Tests
{
	[TestClass]
	public class ActionsTests
	{
		private Actions _actions;
		[TestInitialize]
		public void SetUp()
		{
			_actions = new Actions();
		}
		[TestMethod]
		public void ActionsExecutesResend()
		{
			const string actionName = "Resend";
			const string expected = "Method Resend was successfully executed";

			var result = _actions.ExecuteAction(actionName);

			Assert.IsNotNull(result);
			Assert.IsTrue(!(string.IsNullOrEmpty(result) || string.IsNullOrWhiteSpace(result)));
			Assert.AreEqual(expected, result);
		}
		[TestMethod]
		public void ActionExecutesFix()
		{
			const string actionName = "Fix";
			const string expected = "Method Fix was successfully executed";

			var result = _actions.ExecuteAction(actionName);

			Assert.IsNotNull(result);
			Assert.IsTrue(!(string.IsNullOrEmpty(result) || string.IsNullOrWhiteSpace(result)));
			Assert.AreEqual(expected, result);
		}
		[TestMethod]
		public void ActionExecutesCall()
		{
			const string actionName = "Call";
			const string expected = "Method Call was successfully executed";

			var result = _actions.ExecuteAction(actionName);

			Assert.IsNotNull(result);
			Assert.IsTrue(!(string.IsNullOrEmpty(result) || string.IsNullOrWhiteSpace(result)));
			Assert.AreEqual(expected, result);
		}
		[TestMethod]
		public void ExecutesActionThrowsArgumentNullExc()
		{
			try
			{
				_actions.ExecuteAction(null);
			}
			catch (ArgumentNullException ex)
			{
				Assert.AreEqual("Value cannot be null.\r\nParameter name: methodName", ex.Message);
			}
		}
	}
}
